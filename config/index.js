const https = require('./https'),
    general = require('./general'),
    files = require('./files');

module.exports = {
    dirModule: '/modules/',
    extModule: '.js',
    dirPackage: '/modules/package/',
    request: https,
    general: general,
    files: files
}