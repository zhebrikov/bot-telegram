const dirFiles = 'files';
module.exports = {
    dirFiles: dirFiles,
    extFiles: '.html',
    dirForFile: `${process.cwd()}/${dirFiles}`
}