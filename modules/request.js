module.exports = (mod, config) => {

    const {https, fs} = mod,
        {files, request} = config,
        /**
         * file name generator by time
         * @returns {string}
         */
        getName = () => {
            const time = new Date(),
                name = `${time.getHours()}_${time.getMinutes()}_${time.getDate()}_${time.getMonth()}_${time.getFullYear()}${files.extFiles}`

            return `${files.dirForFile}/${name}`;
        },
        action = () => {

            const fileName = getName();
            return new Promise((resolve, reject) => {
                https.get(request.protocol+request.hostname+request.path, (res) => {

                    let data = '';

                    if (res.statusCode !== 200) {
                        console.error(`Did not get an OK from the server. Code: ${res.statusCode}`);
                        res.resume();
                        return;
                    }

                    res.on('data', chunk => {
                        data += chunk;
                    });


                    res.on('end', (e) => {
                        fs.writeFileSync(
                            fileName,
                            data
                        );
                        resolve(fileName);
                    })

                }).on('error', (e) => {
                    reject(e);
                });
            });

        };

    return {
        action,
    };
}