module.exports = (mod, config) => {
    const {https} = mod,
        sendMessage = (item) => {
            // const text = `[${item.title}](https://sport24.ru${item.href})`;
            https.get(`https://api.telegram.org/bot1004214442:AAF1lGldvNW8mgfUWDs-I8VW9xwlzcFSMVI/sendMessage?chat_id=@zheevfootball&text=${item}&parse_mode=markdown`, (res) => {
                let data = '';
                res.on('data', chunk => {
                    data += chunk;
                });

                res.on('end', (e) => {
                    console.log(data);
                })
            });
        },
        sendMessages = (items) => {
            const message = [],
                lineBreak = encodeURI('\n'),
                itemsLength = items.length;
            for(let i = 0; i < itemsLength; i++)
            {
                const item = items[i];
                message.push(`[${item.title}](https://sport24.ru${item.href})${lineBreak}${lineBreak}`);

                if(message.length > 10)
                {
                    sendMessage(message.join(' '));
                    message.clear();
                }
            }

            sendMessage(message.join(' '));
        };

    return {
        sendMessage,
        sendMessages
    }
}