module.exports = (mod, config) => {

    const { fs, cheerio } = mod,
        getFile = (pathFile) => {
            return fs.readFileSync(pathFile, {
                encoding: 'utf-8'
            });
        },

        checkTitle = title => {
                const blackList = [
                    'жена',
                    'Кейру',
                    'девшука',
                    'разводе',
                    'измена'
                ],
                len = blackList.length;

                for(let i = 0; i < len; i++) {
                    if( title.indexOf(blackList[i]) > -1 ) {
                        return false;
                    }
                }

                return true
        },

        action = filePath => {
            const html = getFile(filePath),
                listLink = [],
                parsing = cheerio.load(html);

            parsing('a._2XCUXj').each((idx, elem) => {

                const title = parsing(elem).text();

                if (checkTitle(title)) {
                    listLink.push({
                        title: title,
                        href: parsing(elem).attr('href'),
                    });
                }
            });

            return listLink;
       };

    return {
        action
    }

}