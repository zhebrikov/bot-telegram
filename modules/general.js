module.exports = (mod, config) => {

    const run = async () => {

        //Добавили метод для массива, для очистки массива
        Array.prototype.clear = function (){
            const array = this;
            while (array.length) {
                array.pop();
            }
        }

        try {
            const fileName = await mod['request'].action();

            const data = await mod['parsing'].action(fileName);

            mod['send'].sendMessages(data);

        } catch (e) {

        }

    };

    return {
        run,
    }
}