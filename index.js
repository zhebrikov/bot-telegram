const config = require('./config'),
    fs = require('fs'),
    mod = {};


const addModule = (path, mod) => {

        const typePath = fs.statSync(path);

        if (!typePath.isDirectory())
            return false;

        const modules = fs.readdirSync(
            path
        );

        for (const module of modules) {
            const namePack = module.split('.').shift(),
                typeItem = fs.statSync(path + module);

            // check of type
            // if the type of this item is a directory, skip
            if (typeItem.isDirectory()) {
                continue;
            }

            // if there addes packages, would to so for this arguments not add
            if (path === config.dirPackage) {
                mod[namePack] = require(path + module);
            } else {
                mod[namePack] = require(path + module)(mod, config);
            }
        }
    },
    checkDirFile = () => {
      
        const dirFiles = fs.statSync(config.files.dirForFile, {
            throwIfNoEntry: false
        });

        if (!dirFiles) {
            fs.mkdirSync(config.files.dirForFile, {
                recursive: true
            });
        }
    };

checkDirFile();

//Add package
addModule(process.cwd() + config.dirPackage, mod);

//Add user module
addModule(process.cwd() + config.dirModule, mod);

mod['general'].run();
